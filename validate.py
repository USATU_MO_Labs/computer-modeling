import math


def _sample_distribution(time_points, T0, T, k):
    """

    Единица времени вычисляется внутри на основе заданного количества отрезков.

    time_points - список временных точек
    T0 - начало временного отрезка
    T - конец временного отрезка
    k - количество отрезков для равномерного разбиения отрезка [T0, T]
    """

    def hash_t(t):
        """
        Преобразование момента времени в индекс отрезка, куда этот момент времени попадает
        """
        segment = (T - T0) / k
        # можно было сделать просто math.floor,
        # но так покрывается случай, когда t = T
        return math.ceil(t / segment) - 1

    # частоты для каждого временного промежутка
    buckets = [0] * k

    for point in map(hash_t, time_points):
        buckets[point] += 1

    # количество временных промежутков с данными количествами событий
    # 0 и все остальные числа - поэтому "max + 1"
    frequencies = [0] * (max(buckets) + 1)
    for count in buckets:
        frequencies[count] += 1

    return frequencies


def _sample_mean(sample):
    """
    Выборочное среднее. 

    Для распределения Пуассона можно считать за параметр интенсивности.
    По сути, рассчет того, сколько в среднем происходило событий за единицу времени.
    """
    avg = 0
    for i in range(len(sample)):
        avg += i * sample[i]

    avg /= sum(sample)

    return avg


def _poisson_distribution(rate, i):
    """
    Функция вероятности распределения Пуассона
    """
    return (math.e**-rate) * (rate**i) / math.factorial(i)


def _pearson_test(sample_distribution, theoretical_distribution):
    """
    Критерий Пирсона
    alpha = 0.05
    """

    chi_observed = sum(map(
        lambda dist: (dist[1] - dist[0])**2 / dist[1],
        zip(sample_distribution, theoretical_distribution)
    ))

    # для alpha = 0.05
    chi_critical_list = [
        3.8, 6.0, 7.8, 9.5, 11.1, 
        12.6, 14.1, 15.5, 16.9, 18.3, 
        19.7, 21, 22.4, 23.7, 25, 
        26.3, 27.6, 28.9, 30.1, 31.4, 
        32.7, 33.9, 35.2, 36.4, 37.7, 
        38.9, 40.1, 41.3, 42.6, 42.8
    ]

    # степени свободы (почему -2?)
    df = max(len(sample_distribution), len(theoretical_distribution)) - 2
    print("Степени свободы: {0}".format(df))
    # если df = 0, то все очень плохо
    chi_critical = chi_critical_list[df - 1]

    result = {
        "chi_observed": chi_observed,
        "chi_critical": chi_critical,
        "null_hypothesis_accepted": chi_observed < chi_critical
    }

    return result


def validate(time_points, T0, T):
    """
    Выполняет проверку данного процесса на соответствие распределению Пуассона

    time_points - список временных точек, процесс
    T0 - начало временного отрезка
    T - конец временного отрезка
    source_rate - интенсивность процесса time_points
    """

    k = math.ceil(T - T0)
    segment_length = (T - T0) / k

    sample_distribution = _sample_distribution(time_points, T0, T, k)
    sample_rate = _sample_mean(sample_distribution)

    total_points = len(time_points)

    # print("sample rate {0}, segment length {1}", [sample_rate, segment_length])

    # изменяем интенсивность, чтобы она совпадала с той,
    # которая использовалась для генерации процесса
    # при генерации интенсивность измерялась для одной секунды;
    # здесь же она измеряется для отрезка, укладывающегося в (T - T0) k раз
    theoretical_rate = sample_rate * segment_length
    print("Выборочное среднее: {0}".format(theoretical_rate))

    theoretical_distribution = [0] * len(sample_distribution)
    for i in range(len(theoretical_distribution)):
        theoretical_distribution[i] = _poisson_distribution(
            theoretical_rate, i) * total_points

    print("Эмпирическое распределение:\n{0}".format(sample_distribution))
    print("Теоретическое распределение:\n{0}".format(theoretical_distribution))

    return _pearson_test(sample_distribution, theoretical_distribution)
