from random import random
import math
# from scipy.optimize import minimize


def generate_time(lam):
    """
    Генерирует длительность задержки между событиями по распределению Пуассона с заданной интенсивностью
    """
    if lam == 0:
        raise ValueError(lam)

    # чтобы было (0, 1], а не [0, 1)
    random_number = 1 - random()

    return -1/lam * math.log(random_number)


def calculate_lambdas_linear(f, min_x, max_x, k):
    """
    Разбивает данную функцию на отрезке [min_x, max_x] на k подинтервалов
    и находит максимумы на этих подинтервалах.

    Работает только на монотонных функциях.

    f - линейная функция одного аргумента
    min_x - начало отрезка
    max_x - конец отрезка
    k - количество отрезков
    """
    lambdas = []

    arg_points = []
    for i in range(0, k + 1):
        arg_i = min_x + (max_x - min_x) / k * i
        arg_points.append(arg_i)

    for i in range(0, k):
        arg_1 = arg_points[i]
        arg_2 = arg_points[i + 1]

        # максимум линейной скалярной функции на отрезке - один из концов отрезка
        lambdas.append(max(arg_1, arg_2))

    return lambdas


"""
# пока что лучше об этой функции забыть;
# а вообще необходимо искать глобальный минимум на отрезке
# простая минимизация тут подходит только для функций без нескольких "холмов"
def scalar_func_min_max(f, min_x, max_x):
    # f перевернутая, для поиска максимума
    f_neg = lambda x: -f(x)

    # method = "bounded"
    # bounds = (min_x, max_x)

    # f_result = minimize_scalar(f, method=method, bounds=bounds)
    # f_neg_result = minimize_scalar(f_neg, method=method, bounds=bounds)

    method = "L-BFGS-B"
    bounds = [(min_x, max_x)]
    x0 = (max_x - min_x) / 2

    f_result = minimize(f, x0, method=method, bounds=bounds)
    f_neg_result = minimize(f_neg, x0, method=method, bounds=bounds)

    if not f_result.success:
        raise ValueError(f_result.message)

    if not f_neg_result.success:
        raise ValueError(f_neg_result.message)

    # min, max
    return (f_result.x, -f_neg_result.x)
"""
