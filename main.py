from random import random
import lib
from validate import validate


def homogeneous_process(T0, T, lam):
    """
    Однородный процесс Пуассона

    T0 - начало временного отрезка
    T - конец временного отрезка
    lam - интенсивность
    """
    time_points = []
    t = T0

    while True:
        t += lib.generate_time(lam)
        if t <= T:
            time_points.append(t)
        else:
            break

    return time_points


def non_homogeneous_process(T0, T, lam_f, lam_max):
    """
    Неоднородный процесс Пуассона

    T0 - начало временного отрезка
    T - конец временного отрезка
    lam_f - функция интенсивности от времени
    lam_max - максимальное значение, которое функция lam_f принимает на отрезке [T0, T]
    """
    time_points = []
    t = T0

    while True:
        t += lib.generate_time(lam_max)
        if t <= T:
            # вероятность возникновения события в данный момент времени
            probability = lam_f(t) / lam_max
            if random() <= probability:
                time_points.append(t)
        else:
            break

    return time_points


def non_homogeneous_process_subintervals(T0, T, k, lam_f, lam_max_arr):
    """
    Неоднородный процесс Пуассона методом интервалов

    T0 - начало временного отрезка
    T - конец временного отрезка
    k - количество интервалов, деление на равные части
    lam_f - функция интенсивности потока от времени
    lam_max_arr - список максимумов функции lam_f для каждого из k временных интервалов
    """
    if len(lam_max_arr) != k:
        raise ValueError("Not enough lambdas in lam_nax_arr")

    t_arr = []
    for i in range(0, k + 1):
        t_i = T0 + (T - T0) / k * i
        t_arr.append(t_i)

    assert(len(t_arr) == len(lam_max_arr) + 1)

    time_points = []

    for i in range(0, k):
        t0 = t_arr[i]
        t = t_arr[i + 1]
        lam_max = lam_max_arr[i]
        local_time_points = non_homogeneous_process(t0, t, lam_f, lam_max)
        time_points.extend(local_time_points)

    return time_points


def main():
    T0, T = 0, 120

    print("Однородный процесс")
    lam = 1
    l = homogeneous_process(T0, T, lam)
    print("Всего точек: {0}".format(len(l)))
    print(l)
    result = validate(l, T0, T)
    print("Хи^2 наблюдаемое: {0}, Хи^2 теоретическое: {1}".format(
        result["chi_observed"], result["chi_critical"]))
    print("Гипотеза H0 {0}".format(
        "подтверждена" if result["null_hypothesis_accepted"] else "опровергнута"))
    print()


    print("Неоднородный процесс")
    def f(t): return 2 + 1 / (t + 1)
    l = non_homogeneous_process(T0, T, f, 3)
    print("Всего точек: {0}".format(len(l)))
    print(l)
    result = validate(l, T0, T)
    print("Хи^2 наблюдаемое: {0}, Хи^2 теоретическое: {1}".format(
        result["chi_observed"], result["chi_critical"]))
    print("Гипотеза H0 {0}".format(
        "подтверждена" if result["null_hypothesis_accepted"] else "опровергнута"))
    print()


    # print("Неоднородный процесс с подинтервалами")
    # k = 10
    # lambdas = lib.calculate_lambdas_linear(f, T0, T, k)
    # l = non_homogeneous_process_subintervals(T0, T, k, f, lambdas)
    # # print(l)
    # print("Всего точек: {0}".format(len(l)))
    # result = validate(l, T0, T)
    # print("Хи^2 наблюдаемое: {0}, Хи^2 теоретическое: {1}".format(
    #     result["chi_observed"], result["chi_critical"]))
    # print("Гипотеза H0 {0}".format(
    #     "подтверждена" if result["null_hypothesis_accepted"] else "опровергнута"))
    # print()


if __name__ == "__main__":
    main()
